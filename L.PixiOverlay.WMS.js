// Leaflet.PixiOverlay.WMS
// version: 0.0.1
// author: Mark Heppner <mark.heppner@appliedis.com>
// license: MIT

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['leaflet', 'pixi.js'], factory);
    } else if (typeof module !== 'undefined') {
        // Node/CommonJS
        module.exports = factory(require('leaflet'), require('pixi.js'));
    } else {
        // Browser globals
        if (typeof window.L === 'undefined') {
            throw new Error('Leaflet must be loaded first');
        }
        if (typeof window.PIXI === 'undefined') {
            throw new Error('Pixi.js must be loaded first');
        }
        factory(window.L, window.PIXI);
    }
}(function (L, PIXI) {

  // globals for all instances of the plugin
  var
    // canvas element ID
    ELEMENT_ID = 'root-pixi-overlay',
    // main renderer object
    RENDERER = PIXI.autoDetectRenderer({
      transparent: true,
      resolution: L.Browser.retina ? 2 : 1,
      antialias: true,
    }),
    // root container that holds child containers of this instance
    ROOT_CONTAINER = new PIXI.Container(),
    // global render function to render the root container
    RENDER_ROOT_CONTAINER = L.Util.throttle(function() {
      // throttle the outer function to prevent duplicate instances of the plugin from simultaneously calling
      // use a 0 timeout and set immediately to true to just catch simultaneous calls
      var render = function() {
        RENDERER.render(ROOT_CONTAINER);
      }

      // actual rendering is requested in an animation frame
      // even though animation frames are
      L.Util.requestAnimFrame(render);
    }, 0, true);

  L.TileLayerPixi = L.TileLayer.extend({

    // https://github.com/manubb/Leaflet.PixiOverlay/blob/0.0.6/L.PixiOverlay.js#L27
    options: {
      // @option padding: Number = 0.1
      // How much to extend the clip area around the map view (relative to its size)
      // e.g. 0.1 would be 10% of map view in each direction
      padding: 0.1,

      pane: 'overlayPane',
    },

    initialize: function (url, options) {
      console.debug('init', this, url, options);

      L.TileLayer.prototype.initialize.call(this, url, options);

      // https://github.com/manubb/Leaflet.PixiOverlay/blob/0.0.6/L.PixiOverlay.js#L41
      L.stamp(this);

      // create a local container for this wms url
      this._pixiContainer = new PIXI.Container();
      this._pixiContainer.layerId = url;
      ROOT_CONTAINER.addChild(this._pixiContainer);

    },

    // https://github.com/manubb/Leaflet.PixiOverlay/blob/0.0.6/L.PixiOverlay.js#L186
    _disableLeafletRounding: function() {
      this._leaflet_round = L.Point.prototype._round;
      L.Point.prototype._round = function() {return this;};
    },

    // https://github.com/manubb/Leaflet.PixiOverlay/blob/0.0.6/L.PixiOverlay.js#L191
    _enableLeafletRounding: function() {
      L.Point.prototype._round = this._leaflet_round;
    },


    // overridden
    // https://github.com/Leaflet/Leaflet/blob/v1.2.0/src/layer/tile/GridLayer.js#L347
    _initContainer: function() {
      console.debug('_initContainer', this);
      // overridden gl
      if (this._container) {
        return;
      }

      // https://github.com/manubb/Leaflet.PixiOverlay/blob/0.0.6/L.PixiOverlay.js#L54
      this._container = document.getElementById(ELEMENT_ID);

      if (!this._container) {
        // no existing canvas exists yet, which means this is the first instance of the plugin
        // create a global element for all other instances to use
        this._container = document.createElement('div');
        this._container.id = ELEMENT_ID;
        L.DomUtil.addClass(this._container, 'leaflet-pixi-overlay');
        this._container.appendChild(RENDERER.view);
        if (this._zoomAnimated) {
          L.DomUtil.addClass(this._container, 'leaflet-zoom-animated');
        }

        this.getPane().appendChild(this._container);
      }

      // https://github.com/manubb/Leaflet.PixiOverlay/blob/0.0.6/L.PixiOverlay.js#L66
      var map = this._map;
      this._initialZoom = map.getMaxZoom();
      this._wgsOrigin = L.latLng([0, 0]);
      this._disableLeafletRounding();
      this._wgsInitialShift = map.project(this._wgsOrigin, this._initialZoom);
      this._enableLeafletRounding();
      this._mapInitialZoom = map.getZoom();
      this._scale = map.getZoomScale(this._mapInitialZoom, this._initialZoom);

    },


    // overridden
    // https://github.com/Leaflet/Leaflet/blob/v1.2.0/src/layer/tile/GridLayer.js#L638
    _update: function (center) {
      console.debug('_update', this, center);
      // gl
      var map = this._map;
      if (!map) { return; }
      var zoom = this._clampZoom(map.getZoom());

      if (center === undefined) { center = map.getCenter(); }
      if (this._tileZoom === undefined) { return; }  // if out of minzoom/maxzoom

      var pixelBounds = this._getTiledPixelBounds(center),
          tileRange = this._pxBoundsToTileRange(pixelBounds),
          tileCenter = tileRange.getCenter(),
          queue = [],
          margin = this.options.keepBuffer,
          noPruneRange = new L.Bounds(tileRange.getBottomLeft().subtract([margin, -margin]),
                                      tileRange.getTopRight().add([margin, -margin]));

      // Sanity check: panic if the tile range contains Infinity somewhere.
      if (!(isFinite(tileRange.min.x) &&
            isFinite(tileRange.min.y) &&
            isFinite(tileRange.max.x) &&
            isFinite(tileRange.max.y))) { throw new Error('Attempted to load an infinite number of tiles'); }

      for (var key in this._tiles) {
        var c = this._tiles[key].coords;
        if (c.z !== this._tileZoom || !noPruneRange.contains(new L.Point(c.x, c.y))) {
          this._tiles[key].current = false;
        }
      }

      // _update just loads more tiles. If the tile zoom level differs too much
      // from the map's, let _setView reset levels and prune old tiles.
      if (Math.abs(zoom - this._tileZoom) > 1) { this._setView(center, zoom); return; }

      // create a queue of coordinates to load tiles from
      for (var j = tileRange.min.y; j <= tileRange.max.y; j++) {
        for (var i = tileRange.min.x; i <= tileRange.max.x; i++) {
          var coords = new L.Point(i, j);
          coords.z = this._tileZoom;

          if (!this._isValidTile(coords)) { continue; }

          if (!this._tiles[this._tileCoordsToKey(coords)]) {
            queue.push(coords);
          }
        }
      }

      // sort tile queue to load tiles in order of their distance to center
      queue.sort(function (a, b) {
        return a.distanceTo(tileCenter) - b.distanceTo(tileCenter);
      });

      if (queue.length !== 0) {
        // if it's the first batch of tiles to load
        if (!this._loading) {
          this._loading = true;
          // @event loading: Event
          // Fired when the grid layer starts loading tiles.
          this.fire('loading');
        }

        // create DOM fragment to append tiles in one batch
        // var fragment = document.createDocumentFragment();
        // var fragment = new PIXI.Container();

        for (i = 0; i < queue.length; i++) {
          this._addTile(queue[i], this._level.el); //fragment);
        }

        // this._level.el.appendChild(fragment);
        // this._level.el.addChild(fragment);
      }

      // https://github.com/manubb/Leaflet.PixiOverlay/blob/0.0.6/L.PixiOverlay.js#L148
      var p = this.options.padding,
        mapSize = this._map.getSize(),
        min = this._map.containerPointToLayerPoint(mapSize.multiplyBy(-p)).round();
      this._bounds = new L.Bounds(min, min.add(mapSize.multiplyBy(1 + p * 2)).round());

      var b = this._bounds,
        container  = this._container,
        size = b.getSize();

      L.DomUtil.setPosition(container, b.min);

      if (!RENDERER.size || RENDERER.size.x !== size.x || RENDERER.size.y !== size.y) {
          RENDERER.resize(size.x, size.y);
          RENDERER.view.style.width = size.x + 'px';
          RENDERER.view.style.height = size.y + 'px';
          RENDERER.size = size;
      }

      // this._pixiContainer.scale.set(this._scale);
      // set the container as the offset from the translated canvas
      ROOT_CONTAINER.position.set(-b.min.x, -b.min.y);

      // new
      this.renderContainer();
    },

    // new
    renderContainer: function () {
      console.debug('renderContainer');
      RENDER_ROOT_CONTAINER();
    },


    // overridden
    // https://github.com/Leaflet/Leaflet/blob/v1.2.0/src/layer/tile/GridLayer.js#L360
    // https://github.com/manubb/Leaflet.PixiOverlay/blob/0.0.6/L.PixiOverlay.js#L144
    _updateLevels: function () {
      console.debug('_updateLevels', this);
      var zoom = this._tileZoom,
        maxZoom = this.options.maxZoom;

      if (zoom === undefined) { return undefined; }

      for (var z in this._levels) {
        // if (this._levels[z].el.children.length || parseInt(z) === zoom) {
        // TODO removed check for children elements - previous zoom levels would get stuck
        if (parseInt(z) === zoom) {
          // this._levels[z].el.style.zIndex = maxZoom - Math.abs(zoom - z);
          this._levels[z].el._zIndex = maxZoom - Math.abs(zoom - parseInt(z));

          // organize by internal z index
          this._pixiContainer.children.sort(function(a, b) {
            return a._zIndex - b._zIndex;
          });
          this.renderContainer();

          this._onUpdateLevel(z);
        } else {
          // DomUtil.remove(this._levels[z].el);
          this._levels[z].el.destroy();
          this._removeTilesAtZoom(z);
          this._onRemoveLevel(z);
          delete this._levels[z];
        }
      }

      var level = this._levels[zoom],
        map = this._map;

      if (!level) {
        level = this._levels[zoom] = {};

        // level.el = DomUtil.create('div', 'leaflet-tile-container leaflet-zoom-animated', this._container);
        // level.el.style.zIndex = maxZoom;
        level.el = new PIXI.Container();
        this._pixiContainer.addChild(level.el);

        level.origin = map.project(map.unproject(map.getPixelOrigin()), zoom).round();
        level.zoom = zoom;

        this._setZoomTransform(level, map.getCenter(), map.getZoom());

        // force the browser to consider the newly added element for transition
        // Util.falseFn(level.el.offsetWidth);

        this._onCreateLevel(level);
      }

      this._level = level;
      return level;
    },

    // overridden
    // https://github.com/Leaflet/Leaflet/blob/v1.2.0/src/layer/tile/GridLayer.js#L588
    _setZoomTransform: function (level, center, zoom) {
      console.debug('_setZoomTransform', this, level, center, zoom);
      var scale = this._map.getZoomScale(zoom, level.zoom),
        translate = level.origin.multiplyBy(scale)
              .subtract(this._map._getNewPixelOrigin(center, zoom)).round();

      level.el.x = translate.x;
      level.el.y = translate.y;

      level.el.scale.x = scale;
      level.el.scale.y = scale;

      // if (L.Browser.any3d) {
      //   L.DomUtil.setTransform(level.el, translate, scale);
      // } else {
      //   L.DomUtil.setPosition(level.el, translate);
      // }
    },

    // overridden
    // https://github.com/Leaflet/Leaflet/blob/v1.2.0/src/layer/tile/TileLayer.js#L128
    createTile: function (coords, done) {
      console.debug('createTile', this, coords, done);
      // tl
      var src = this.getTileUrl(coords),
        tile = new PIXI.Sprite.fromImage(src);

      // DomEvent.on(tile, 'load', Util.bind(this._tileOnLoad, this, done, tile));
      // DomEvent.on(tile, 'error', Util.bind(this._tileOnError, this, done, tile));
      tile._texture.baseTexture.on('loaded', L.Util.bind(this._tileOnLoad, this, done, tile));
      tile._texture.baseTexture.on('error', L.Util.bind(this._tileOnError, this, done, tile));
      return tile;
    },

    // overridden
    // https://github.com/Leaflet/Leaflet/blob/v1.2.0/src/layer/tile/GridLayer.js#L780
    _initTile: function (tile) {
      console.debug('_initTile', this, tile);
      // gl
      var tileSize = this.getTileSize();
      tile.width = tileSize.x;
      tile.height = tileSize.y;
      tile.alpha = 1;
    },

    // overridden
    // https://github.com/Leaflet/Leaflet/blob/v1.2.0/src/layer/tile/GridLayer.js#L802
    _addTile: function (coords, container) {
      console.debug('_addTile', this, coords, container);
      // gl
      var tilePos = this._getTilePos(coords),
        key = this._tileCoordsToKey(coords);

      var tile = this.createTile(this._wrapCoords(coords), L.Util.bind(this._tileReady, this, coords));

      this._initTile(tile);

      // if createTile is defined with a second argument ("done" callback),
      // we know that tile is async and will be ready later; otherwise
      if (this.createTile.length < 2) {
        // mark tile as ready, but delay one frame for opacity animation to happen
        L.Util.requestAnimFrame(L.Util.bind(this._tileReady, this, coords, null, tile));
      }

      // DomUtil.setPosition(tile, tilePos);
      tile.x = tilePos.x;
      tile.y = tilePos.y;

      // save tile in cache
      this._tiles[key] = {
        el: tile,
        coords: coords,
        current: true
      };

      // container.appendChild(tile);
      container.addChild(tile);
      // @event tileloadstart: TileEvent
      // Fired when a tile is requested and starts loading.
      this.fire('tileloadstart', {
        tile: tile,
        coords: coords
      });
    },


    // overridden
    // https://github.com/Leaflet/Leaflet/blob/v1.2.0/src/layer/tile/GridLayer.js#L764
    _removeTile: function (key) {
      console.debug('_removeTile', this, key, this._tiles[key].coords.z);
      var tile = this._tiles[key];
      if (!tile) { return; }

      // DomUtil.remove(tile.el);
      tile.el.destroy();

      delete this._tiles[key];

      // @event tileunload: TileEvent
      // Fired when a tile is removed (e.g. when a tile goes off the screen).
      this.fire('tileunload', {
        tile: tile.el,
        coords: this._keyToTileCoords(key)
      });
    },

    // overridden
    // https://github.com/Leaflet/Leaflet/blob/v1.2.0/src/layer/tile/GridLayer.js#L446
    _removeTilesAtZoom: function (zoom) {
      console.debug('_removeTilesAtZoom', zoom, this._tiles, this._levels);
      for (var key in this._tiles) {
        if (this._tiles[key].coords.z !== parseInt(zoom)) {
          continue;
        }
        this._removeTile(key);
      }
    },

    // overridden
    // https://github.com/Leaflet/Leaflet/blob/v1.2.0/src/layer/tile/GridLayer.js#L461
    _invalidateAll: function() {
      console.debug('_invalidateAll', this);
      for (var z in this._levels) {
        // DomUtil.remove(this._levels[z].el);
        this._levels[z].el.destroy();
        this._onRemoveLevel(z);
        delete this._levels[z];
      }
      this._removeAllTiles();

      this._tileZoom = null;
    },

    // overridden
    // https://github.com/Leaflet/Leaflet/blob/v1.2.0/src/layer/tile/GridLayer.js#L835
    _tileReady: function (coords, err, tile) {
      if (!this._map) { return; }

      if (err) {
        // @event tileerror: TileErrorEvent
        // Fired when there is an error loading a tile.
        this.fire('tileerror', {
          error: err,
          tile: tile,
          coords: coords
        });
      }

      var key = this._tileCoordsToKey(coords);

      tile = this._tiles[key];
      if (!tile) { return; }

      tile.loaded = +new Date();
      if (this._map._fadeAnimated) {
        // DomUtil.setOpacity(tile.el, 0);
        tile.el.alpha = 0;
        L.Util.cancelAnimFrame(this._fadeFrame);
        this._fadeFrame = L.Util.requestAnimFrame(this._updateOpacity, this);
      } else {
        tile.active = true;
        this._pruneTiles();
      }

      if (!err) {
        // DomUtil.addClass(tile.el, 'leaflet-tile-loaded');

        // @event tileload: TileEvent
        // Fired when a tile loads.
        this.fire('tileload', {
          tile: tile.el,
          coords: coords
        });
      }

      if (this._noTilesToLoad()) {
        this._loading = false;
        // @event load: Event
        // Fired when the grid layer loaded all visible tiles.
        this.fire('load');

        if (L.Browser.ielt9 || !this._map._fadeAnimated) {
          L.Util.requestAnimFrame(this._pruneTiles, this);
        } else {
          // Wait a bit more than 0.2 secs (the duration of the tile fade-in)
          // to trigger a pruning.
          setTimeout(L.Util.bind(this._pruneTiles, this), 250);
        }
      }
    },

    // overridden
    // https://github.com/Leaflet/Leaflet/blob/v1.2.0/src/layer/tile/GridLayer.js#L306
    _updateOpacity: function () {
      if (!this._map) { return; }

      // IE doesn't inherit filter opacity properly, so we're forced to set it on tiles
      if (L.Browser.ielt9) { return; }

      //DomUtil.setOpacity(this._container, this.options.opacity);
      this._pixiContainer.alpha = this.options.opacity;

      var now = +new Date(),
          nextFrame = false,
          willPrune = false;

      for (var key in this._tiles) {
        var tile = this._tiles[key];
        if (!tile.current || !tile.loaded) { continue; }

        var fade = Math.min(1, (now - tile.loaded) / 200);

        //DomUtil.setOpacity(tile.el, fade);
        tile.el.alpha = fade;
        if (fade < 1) {
          nextFrame = true;
        } else {
          if (tile.active) {
            willPrune = true;
          } else {
            this._onOpaqueTile(tile);
          }
          tile.active = true;
        }
      }

      if (willPrune && !this._noPrune) { this._pruneTiles(); }

      if (nextFrame) {
        L.Util.cancelAnimFrame(this._fadeFrame);
        this._fadeFrame = L.Util.requestAnimFrame(this._updateOpacity, this);
      }

      this.renderContainer();
    },


  });

  // @factory L.pixiOverlay(drawCallback: function, pixiContainer: PIXI.Container, options?: L.PixiOverlay options)
  // Creates a PixiOverlay with the given arguments.
  L.tileLayerPixi = function (url, options) {
    return L.Browser.canvas ? new L.TileLayerPixi(url, options) : null;
  };

  L.tileLayerWMSPixi = function (url, options) {
    return L.Browser.canvas ? new L.TileLayerWMSPixi(url, options) : null;
  };

  // expose globals
  L.tileLayerPixiRootContainer = ROOT_CONTAINER;
  L.tileLayerPixiRenderRootContainer = RENDER_ROOT_CONTAINER;
  L.tileLayerPixiRenderer = RENDERER;

}));
